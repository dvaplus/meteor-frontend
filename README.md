
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--,
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    -----------------------------------------------------------------

## Instructions

Welcome to your **Meteor frontend boilerplate** on Cloud9 IDE!

1. Open [https://c9.io/dashboard.html](https://c9.io/dashboard.html)
2. Press large **`+`** button
3. Use `git@gitlab.com:dvaplus/meteor-frontend.git` in *URL* field
4. Fill in *Workspace* name and *Description* fields
5. Choose **Meteor** template

![screenshot](http://a.pomf.cat/similf.png)
6. Press *Create workspace* button


## Upgrade to Meteor 1.3

1. Move data from `packages.json` and `postcss.json` to `package.json`
2. Run in terminal window
```
mkdir -p node_modules; npm update -g; npm update -g npm; npm install; meteor update --release=METEOR@1.3-modules-beta.5; meteor remove stylus meteorhacks:npm npm-container juliancwirko:postcss; rm -rf packages/npm-container; meteor add standard-minifiers-js juliancwirko:postcss@1.0.0-beta.1 juliancwirko:s-jeet; if [ ! -f ".gitignore" ]; then echo 'node_modules/*' > .gitignore; fi; touch node_modules/.gitkeep; git add -f .c9/project.settings .c9/.nakignore node_modules/.gitkeep; rm -f packages.json postcss.json;
```
Wait 10-15 minutes to finish
3. Update `1. Run project.run` runner


## Support & Documentation

#### Meteor
- http://docs.meteor.com/

#### PostCSS plugins included
- CSS Next - http://cssnext.io/
- Rucksack - http://simplaio.github.io/rucksack/docs/
- CSS Nano - http://cssnano.co/optimisations/
- LostGrid - https://github.com/peterramsing/lost
- https://github.com/jonathantneal/postcss-short-spacing
- https://github.com/jonathantneal/postcss-short-font-size
- https://github.com/andrepolischuk/postcss-pseudo-class-any-button
- https://au.si/css-container-element-queries
- https://github.com/travco/postcss-extend
- https://github.com/postcss/postcss-media-minmax
- http://cssnano.co/optimisations/
- https://github.com/ben-eb/postcss-svgo
- https://github.com/lukelarsen/postcss-hidden
- https://github.com/jedmao/postcss-nested-vars
- https://github.com/andyjansson/postcss-conditionals
- https://github.com/outpunk/postcss-each
- https://github.com/antyakushev/postcss-for
- https://github.com/GitScrum/postcss-at-rules-variables
- https://github.com/andyjansson/postcss-functions
